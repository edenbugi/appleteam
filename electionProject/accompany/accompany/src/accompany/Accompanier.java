package accompany;

public class Accompanier {
	private static Accompanier first = null;
	private Accompanier next = null;
	private int ID;
	private int visited = 0;
	
	public Accompanier(int id, Accompanier next){
		Accompanier.first = this;
		this.next = next;
		this.ID = id;
	}
	
	public boolean hasNext(){
		if(this.next != null)
			return true;
		return false;
	}
	
	public Accompanier next(){
		return next;
	}
	
	public void setNext(Accompanier next){
		this.next = next;
	}
	
	public Accompanier findID(int id){
		return findID(id,Accompanier.first);
	}
	
	private Accompanier findID(int id, Accompanier current){
		if(id == current.ID){
			return current;
		}
		if(current.hasNext()){
			return findID(id, current.next);
		}
		return null;
	}

	public int getVisited() {
		return visited;
	}

	public void setVisited(int visited) {
		this.visited = visited;
	}
}
