package PreVote;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Program {

	public Program() throws FileNotFoundException {
		int choise;

		Scanner s = new Scanner(System.in);

		do{
		System.out
				.println("please choose what file you would like to create:\n");
		System.out.println("1.parties file");
		System.out.println("2.voters file");
		System.out.println("3.to exit");
		choise = s.nextInt();

		
			switch (choise) {
			case 1:{
				File f = new File("politicalParty.txt");
	
				int numOfParties;
	
				PrintWriter pw = new PrintWriter(f);
				System.out.println("please enter how much parties you want create");
				numOfParties = s.nextInt();
				s.nextLine();
				pw.println(numOfParties);
	
				for (int i = 0; i < numOfParties; i++) {
					System.out.println("Please enter the name of the party");
					String partyName = s.nextLine();
					pw.println(partyName);
				}
				pw.close();
				break;
			}
			case 2:{
				File f = new File("citizens.txt");
	
				int numOfVoters;
	
				PrintWriter pw = new PrintWriter(f);
				System.out.println("please enter how many voters you want create");
				numOfVoters = s.nextInt();
				s.nextLine();
				pw.println(numOfVoters);
	
				for (int i = 0; i < numOfVoters; i++) {
					System.out.println("Please enter the name of the voter");
					String voterName = s.nextLine();
					pw.println(voterName);
					System.out.println("Please enter the ID of " + voterName);
					String voterID = s.nextLine();
					pw.println(voterID);
					System.out.println("Please enter the area " + voterName
							+ " belongs to");
					String voterArea = s.nextLine();
					pw.println(voterArea);
				}
				pw.close();
				break;
			}
			}
		} while(choise != 3);
		
		s.close();
	}

}
