package VoteGUI;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

@SuppressWarnings("serial")
public class VotePanel extends JPanel {
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	protected final int HIGH = 95, WIDTH = 100, WIDTH_FROM_BUTTONS = 10, ZERO = 0, HIGH_FROM_BUTTONS = 20, NULL = -1; // constant locations.
	private JButton nextButton = new JButton("Next");
	private JButton exitButton = new JButton("Exit & Send Results");
	private JTextField idField = new JTextField(9);
	private int stage = 0; // 0 for enter id, 1 for vote after verify.
	protected SpringLayout theLayout = new SpringLayout();
	private int[] ids;
	private boolean[] voted;
	private ButtonGroup group = new ButtonGroup();
	private JRadioButton[] allpoliticals;
	JPanel panel = new JPanel(new GridLayout(15,15));
	private static String[] politicalParty;
	private JLabel idLabel = new JLabel("Enter your id here:");
	private boolean isDone = false;
	
	public VotePanel() throws Exception{
		this.ids = getIDs();
		this.voted =  new boolean[ids.length];// boolean in java initiate to false.;
		politicalParty = getPoliticalParties();
		resetVotes();
		idLabel.setForeground(Color.yellow);
		politicalPartysToRadioButtons(politicalParty);
		this.setLayout(theLayout);
		this.setPreferredSize(new Dimension(screenSize.width / 2, (int)(screenSize.height)));
		this.setBackground(new Color(0,0,50));
		idField.setFont(new Font("Ariel", 20, 20));
		add(nextButton);
		add(exitButton);
		add(idLabel);
		panel.setSize(150, 800);
		for(int i = 0; i < politicalParty.length; i++){
			allpoliticals[i].setForeground(Color.yellow);
			allpoliticals[i].setBackground(new Color(0,0,50));
			panel.add(allpoliticals[i]);
			allpoliticals[i].setVisible(false);
		}
		panel.setVisible(false);
		panel.setBackground(new Color(0,0,50));
		add(idField);
		setLocation(theLayout);
		this.setVisible(false);
	    add(panel);
		
		
		exitButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				exitProgram();
			}
		});
		
		nextButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				switch (stage){
				case 0:
					try{
						int tempIDIndex = findID(Integer.parseInt(idField.getText()));
						if(tempIDIndex != NULL && !didVote(tempIDIndex)) {
							makeVisableNextStage(true);
							idField.setText("");
							stage++;
							setVoted(tempIDIndex);
						}
					}
					catch (Exception e){};
					break;
				case 1:
					int answer = JOptionPane.showConfirmDialog(nextButton,"Are you sure you want to vote for: " + allpoliticals[getVoteIndexFromUser()].getText(),"Confirm",JOptionPane.YES_NO_OPTION);
					if(answer == JOptionPane.YES_OPTION){
						stage--;
						addVote();
						makeVisableNextStage(false);					
						break;
					}
				}
					
				
			}
		});
		
	}
	
	private void makeVisableNextStage(boolean flag){
		for(int i = 0; i < allpoliticals.length; i++)
			allpoliticals[i].setVisible(flag);
		idField.setVisible(!flag);
		idField.setFocusable(!flag);
		idField.setFocusCycleRoot(!flag);
		idLabel.setVisible(!flag);
		panel.setVisible(flag);
	}
	
	private int getVoteIndexFromUser(){
		for(int i = 0; i < allpoliticals.length; i++)
			if(allpoliticals[i].isSelected())
				return i;
		return NULL;
	}
	
	private void addVote(){
		for(int i = 0; i < allpoliticals.length; i++)
			if(allpoliticals[i].isSelected()){
				try{
					addVoteToFile(allpoliticals[i].getText());
				}
				catch (Exception e){};
			}
	}
	
	private void politicalPartysToRadioButtons(String[] politicalParty){
		allpoliticals = new JRadioButton[politicalParty.length];
		for(int i = 0; i<allpoliticals.length; i++)
		{
			allpoliticals[i] = new JRadioButton(politicalParty[i]);
			group.add(allpoliticals[i]);
		}
	}
	
	private void setVoted(int index){
		this.voted[index] = true;
	}
	
	private boolean didVote(int idLocation){
		return this.voted[idLocation];
	}
	
	private int findID(int id) {
		for(int i = 0; i < ids.length; i++)
			if(ids[i] == id)
				return i;
		return NULL;
	}
	
	private void exitProgram(){
		String password = JOptionPane.showInputDialog(null, null, "Enter Password", 0); 
		if(password.equals("112")){
			isDone = true;
			this.setVisible(false);
		}
	}
	
	private void setLocation(SpringLayout theLayout){
		theLayout.putConstraint(SpringLayout.WEST, this.exitButton, screenSize.width / 2 - 200  , SpringLayout.WEST, this);
		theLayout.putConstraint(SpringLayout.NORTH, this.exitButton, screenSize.height - 150  , SpringLayout.NORTH, this);
		
		theLayout.putConstraint(SpringLayout.EAST, this.nextButton, -WIDTH_FROM_BUTTONS  , SpringLayout.WEST, this.exitButton);
		theLayout.putConstraint(SpringLayout.NORTH, this.nextButton, ZERO  , SpringLayout.NORTH, this.exitButton);
		
		theLayout.putConstraint(SpringLayout.WEST, this.idField, (int) screenSize.getWidth()/7  , SpringLayout.WEST, this);
		theLayout.putConstraint(SpringLayout.NORTH, this.idField, (int) (screenSize.getHeight()/5)  , SpringLayout.NORTH, this);
		
		theLayout.putConstraint(SpringLayout.WEST, this.idLabel, (int) screenSize.getWidth()/7  , SpringLayout.WEST, this);
		theLayout.putConstraint(SpringLayout.SOUTH, this.idLabel, ZERO  , SpringLayout.NORTH, this.idField);

	}
	
	private void resetVotes() throws IOException {
		int i = 0;
		File f = new File("result.txt");
		FileWriter fw = new FileWriter(f);
		fw.write(politicalParty.length + "\n");
		for(i = 0; i < politicalParty.length; i++){
			fw.write(politicalParty[i] + "\n");
			fw.write("0\n");
		}
		fw.close();	
	}
	
	private static void addVoteToFile(String politicalParty) throws Exception{
		//adding vote to specific political party.
		int i = 0;
		String[] votes = getVotesFromFile("result.txt");
		while(votes[i].equals(politicalParty)){
			i+=2;
		}
		votes[i+1] = Integer.toString(Integer.parseInt(votes[i+1]) + 100);
		/* Add 100 votes per 1 person, otherwise we need to enter at least 120 people in here and it takes really long time..
		 * so it should be +1 and not +100 but just for the demo system we entered +100. hope you will accept it.
		*/
		writeVotesToFile("result.txt", votes);
	}
	
	private static String[] getVotesFromFile(String fileName) throws Exception{
		//file will be number of political parties, and then each row political party and next row the vote count for it.
		File f = new File(fileName);
		Scanner s;
		int i = 0;
		String[] allVotes;
		if(!f.exists())
			throw new Exception("File doesn't exist");
		s = new Scanner(f);
		allVotes = new String[s.nextInt()*2]; //number of political parties x2 1 for name, 1 for votes.
		s.nextLine();
		while(s.hasNext()){
			allVotes[i++] = s.nextLine();
		}
		s.close();
		return allVotes;
	}
	
	private static void writeVotesToFile(String fileName, String[] votes) throws IOException{
		//writing first number of political parties, then 2 rows per 1, 1- name 2- vote count.
		File f = new File(fileName);
		FileWriter fw = new FileWriter(f);
		int i;
		fw.write(votes.length/2 + "\n");
		for(i = 0; i < votes.length; i++){
			fw.write(votes[i] + "\n");
		}
			
		fw.close();
	}
	
	private static int[] getIDs() throws Exception {
		File f = new File("citizens.txt");
		Scanner s;
		int i = 0;
		int[] allIDs;
		if(!f.exists())
			throw new Exception("File doesn't exist");
		s = new Scanner(f);
		allIDs = new int[s.nextInt()]; //number of IDs.
		s.nextLine();
		while(s.hasNext()){
			s.nextLine();
			allIDs[i++] = s.nextInt();
			s.nextLine();
			s.nextLine();
		}
		s.close();
		return allIDs;
	}
	
	private static String[] getPoliticalParties() throws Exception {
		//political parties will have in first field Name.
		File f = new File("politicalParty.txt");
		String[] politicalParty;
		Scanner s;
		int i = 0;
		
		if(f.exists()) {
			 s = new Scanner(f);
		}
		else
			throw new Exception("File doesn't exist");
		politicalParty = new String[s.nextInt()];
		s.nextLine();
		while(s.hasNext()){
			try{
			politicalParty[i++] = s.nextLine();
			}
			catch (Exception e) {}
		}
		s.close();
		return politicalParty;
	}

	public boolean isDone() {
		return isDone;
	}

}
