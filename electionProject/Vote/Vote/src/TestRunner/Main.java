package TestRunner;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import DivisionMandats.CountingAndPublishing;
import PreVote.Program;
import VoteGUI.VotePanel;


public class Main {
	
	public static void main(String[] args) throws Exception {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		@SuppressWarnings("unused")
		Program preVote  = new Program();
		
		JFrame myFrame = new JFrame();
		myFrame.setUndecorated(true);
		myFrame.setVisible(true);
		myFrame.setSize(screenSize);
		VotePanel votePnl = new VotePanel();
		myFrame.add(votePnl);
		votePnl.setVisible(true);
		while(!votePnl.isDone()){
			System.out.println("");
		}
		myFrame.setVisible(false);
		CountingAndPublishing finalPart = new CountingAndPublishing();
		finalPart.calculateResults();
		finalPart.publishResults();
	}
}
