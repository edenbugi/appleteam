package CountingProcess; /*��� �� ���� ����� �� ����� ����� ������. ����� ��� ����� ������ ���� ���� ���� ����� ��� ����� �������, ��� ���� ��� �������� �� ���� ��� ������ �� ���  ����� ����� ��� ����� ������� ����� ������� ����� ����*/

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class Area {

	Scanner st = new Scanner(System.in);

	private String area;
	private int ballotBoxNum;
	private static final int NUM_OF_PARTIES = 6;
	private Party [] partyArr;
	public static final char NAME_TOKEN = ']';
	public static final char COLON_TOKEN = ':';
	public static Party [] allParties;

	public static void main(String[] args) throws IOException {

		//Predefined parties array for the results
		allParties = new Party [] {new Party ("Likud", 0),
				new Party ("Yesh Atid", 0),
				new Party ("Zionist Union", 0),
				new Party ("Meretz", 0),
				new Party ("Joint List", 0),
				new Party ("The Jewish Home", 0)};

		//Predefined parties array respectively to the areas array (names with initials)
		Party [] partyJAD = new Party [] {new Party ("Likud", 6),
				new Party ("Yesh Atid", 8),
				new Party ("Zionist Union", 11),
				new Party ("Meretz", 3),
				new Party ("Joint List", 5),
				new Party ("The Jewish Home", 4)};

		Party [] partyArrSouth = new Party [] {new Party ("Likud", 15),
				new Party ("Yesh Atid", 4),
				new Party ("Zionist Union", 2),
				new Party ("Meretz", 5),
				new Party ("Joint List", 3),
				new Party ("The Jewish Home", 9)};

		Party [] partyArrNorth = new Party [] {new Party ("Likud", 18),
				new Party ("Yesh Atid", 11),
				new Party ("Zionist Union", 2),
				new Party ("Meretz",8),
				new Party ("Joint List", 5),
				new Party ("The Jewish Home",2)};

		Party [] partyArrTAMA = new Party [] {new Party ("Likud", 9),
				new Party ("Yesh Atid", 10),
				new Party ("Zionist Union", 5),
				new Party ("Meretz", 2),
				new Party ("Joint List", 1),
				new Party ("The Jewish Home", 3)};	

		Party [] partyArrSCO = new Party [] {new Party ("Likud", 8),
				new Party ("Yesh Atid", 6),
				new Party ("Zionist Union", 5),
				new Party ("Meretz", 4),
				new Party ("Joint List", 3),
				new Party ("The Jewish Home", 3)};

		Party [] partyArrHAC = new Party [] {new Party ("Likud", 8),
				new Party ("Yesh Atid", 7),
				new Party ("Zionist Union", 5),
				new Party ("Meretz", 2),
				new Party ("Joint List", 1),
				new Party ("The Jewish Home", 1)};

		Party [] partyArrSharon = new Party [] {new Party ("Likud", 7),
				new Party ("Yesh Atid", 7),
				new Party ("Zionist Union", 8),
				new Party ("Meretz", 5),
				new Party ("Joint List", 2),
				new Party ("The Jewish Home", 6)};

		//Predefined areas array
		Area [] areaArr = {new Area("Jerusalem And Degradation", 2, partyJAD),
				new Area("South", 5, partyArrSouth),
				new Area("North", 5, partyArrNorth),
				new Area("Tel Aviv Metropolitan Area", 5, partyArrTAMA),
				new Area("Southern Coastal Olain", 5, partyArrSCO),
				new Area("Hifa And Carmel", 5, partyArrHAC),
				new Area("Sharon Plain", 5, partyArrSharon)};


		for(int i=0; i<areaArr.length; i++){
			saveInfoToFile(areaArr[i], "Area - " + areaArr[i].getArea() + ".txt");
			sumResults(readInfoFromFile("Area - " + areaArr[i].getArea() + ".txt"));
		}

		partySort(allParties);
		System.out.println("The results: ");
		for(int i=0; i<allParties.length; i++){
			System.out.println(allParties[i]);
		}
		System.out.println("\nWinning party: " + allParties[allParties.length-1]);
	}

	public Area(String area, int ballotBoxNum, Party [] partyArr){

		setArea(area);
		setBallotBoxNum(ballotBoxNum);
		setPartyArr(partyArr);
	}

	public static void saveInfoToFile(Area a, String fileName)  throws IOException{
		PrintWriter pw = new PrintWriter(fileName);
		pw.println("[Area: " + a.getArea() + "\r\n"
				+ "Ballot Box Number: " + a.getBallotBox() + "\r\n"
				+ "Number Of Parties: " + NUM_OF_PARTIES + "]");
		for(int i=0; i<NUM_OF_PARTIES; i++)
			pw.println(a.getPartyArr()[i]);
		pw.close();
	}


	public void setArea(String areaID) {
		this.area = areaID;
	}

	public void setBallotBoxNum(int ballotBoxNum) {
		this.ballotBoxNum = ballotBoxNum;
	}

	public void setPartyArr(Party [] partyArr) {
		this.partyArr = partyArr;
	}

	public String getArea() {
		return area;
	}

	public int getBallotBox() {
		return ballotBoxNum;
	}

	public Party [] getPartyArr(){
		return partyArr;
	}

	public String toString (){
		String str =  "Area: " + area +
				", Ballot Box Number: " + ballotBoxNum +
				", Number Of Parties: " +
				NUM_OF_PARTIES;
		for(int i=0; i<partyArr.length; i++)
			System.out.println(partyArr[i]);
		return str;
	}


	public static Party [] readInfoFromFile(String fileName) throws IOException{
		File f = new File(fileName);
		Scanner in = new Scanner(f);
		String parName = "";
		int votes = 0;
		String [] partiesAndVotes = new String [NUM_OF_PARTIES*2];
		Party [] partiesArr = new Party [NUM_OF_PARTIES];
		//Reading the parties and the number of voters from the file to an array
		while(in.hasNext()){
			String s = in.nextLine();
			for(int i=0; i<s.length(); i++){
				if(s.charAt(i) == NAME_TOKEN){
					int j = 0;
					while(in.hasNext()){
						partiesAndVotes[j] = in.nextLine();
						j++;
					}
				}
			}
		}
		in.close();
		//Creating an array that will hold 
		for(int i=0, j=0; i<partiesAndVotes.length; i++){
			//System.out.println(partiesAndVotes[i]);
			if(i%2==0)
				parName = partiesAndVotes[i];
			else{
				votes = (Integer.parseInt(partiesAndVotes[i]));
				partiesArr[j] = new Party(parName, votes);
				j++;
			}
		}
		return partiesArr;
	}

	public static void sumResults (Party [] arr){
		int sum = 0;
		for(int i=0; i<arr.length; i++){
			sum = allParties[i].getNumOfVoters();
			sum += arr[i].getNumOfVoters();
			allParties[i].setNumOfVoters(sum);
		}
	}

	public static void partySort(Party [] arr){
		int pass = 1;
		boolean exchanges = true;
		while (pass < arr.length && exchanges) {
			exchanges = false;
			for (int i = 0; i < arr.length - pass; i++) {
				if (arr[i].getNumOfVoters() > arr[i + 1].getNumOfVoters()) {
					swap(arr, i, i + 1);
					exchanges = true;
				} // end if
			} // end for
			pass = pass + 1;
		} // end while
	} // end function bubble_sort

	public static void swap(Party [] arr, int from, int to) {
		Party temp = arr[from];
		arr[from] = arr[to];
		arr[to] = temp;
	}

}

class Party{

	private String partyName;
	private int numOfVoters;

	public Party(String partyName, int numOfVoters){
		setPartyName(partyName);
		setNumOfVoters(numOfVoters);
	}

	public String getPartyName(){
		return partyName;
	}

	public int getNumOfVoters(){
		return numOfVoters;
	}

	public void setNumOfVoters(int numOfVoters) {
		this.numOfVoters = numOfVoters;
	}

	public void setPartyName(String name) {
		this.partyName = name;
	}

	public String toString(){
		return partyName + "\r\n" + numOfVoters;
	}
}
