package DivisionMandats;

import java.io.File;
import java.util.Scanner;


public class CountingAndPublishing {

	String parties[];
	int votesForAllParties[];
	int mandatsForAllParties[];
	int numOfVotes;
	int numOfMandats;
	
	public CountingAndPublishing() throws Exception	//constructor
	{
		getDataFromFile();
		this.mandatsForAllParties=new int[votesForAllParties.length];
	}
	
	private void getDataFromFile() throws Exception{
		File f = new File("result.txt");
		Scanner s;
		int i = 0, temp;
		if(!f.exists())
			throw new Exception("File doesn't exist");
		s = new Scanner(f);
		temp =  s.nextInt();
		parties = new String[temp];
		votesForAllParties = new int[parties.length];
		s.nextLine();
		while(s.hasNext()){
			parties[i] = s.nextLine();
			votesForAllParties[i++] = s.nextInt();
			s.nextLine();
		}
		s.close();
	}
	
	private void sumVotes()			//Sum all the votes from all the political parties
	{
		numOfVotes=0;
		for (int i=0;i<votesForAllParties.length;i++)
			numOfVotes+=votesForAllParties[i];
	}
	
	private void sumMandats()		//Check how many mandats have already been given
	{
		numOfMandats=0;
		for (int i=0;i<mandatsForAllParties.length;i++)
			numOfMandats+=mandatsForAllParties[i];
	}
	
	private boolean over2Percent (int votesForParty, int totalVotes)			//If a party did not get 2% of the votes they are not eligible for the Knesset		
	{
		if ((votesForParty*100/totalVotes)>2)			
			return true;
		return false;
	}
	
	private int votesForMandat(int remainingMandats)			//Check how many votes are needed to get 1 mandat
	{
		sumVotes();
		return (numOfVotes/remainingMandats);
	}
	
	private void remainingVotes(int votesForMandat)			//After given the last whole mandat, how many spare votes the political parties have
	{
		for (int i=0;i<votesForAllParties.length;i++)
			votesForAllParties[i]=votesForAllParties[i]%votesForMandat;
	}
	
	private void mandatsForParties(int votesPerMandat)		//initiate how many whole mandats each party should get based on their votes and the votes needed to get a mandat
	{
		for (int i=0;i<votesForAllParties.length;i++)
			mandatsForAllParties[i]+=votesForAllParties[i]/votesPerMandat;
	}
	
	private int mostRemainingVotes()							//Check which political party has the most spare votes after getting the last whole mandat
	{
		int maxVal=0;
		int maxKey=0;
		for (int i=0;i<votesForAllParties.length;i++)
			if (votesForAllParties[i]>maxVal)
			{
				maxKey=i;
				maxVal=votesForAllParties[i];
			}
		return maxKey;
	}
	
	public void calculateResults()			//Combine all the above methods to calculate the final result before publishing them
	{
		int votesPerMandat;
		int remainingMandats; 
		sumVotes();
		for (int i=0;i<mandatsForAllParties.length;i++)
			if (!(over2Percent(votesForAllParties[i], numOfVotes)))
			{
				mandatsForAllParties[i]=0;			//No mandats for the party unless it has 2% of the votes
				votesForAllParties[i]=0;			//Dont count their votes
			}
		sumVotes();
		votesPerMandat = votesForMandat(120);
		mandatsForParties(votesPerMandat);
		sumMandats();

		
		if (numOfMandats!=120)
		{
			remainingVotes(votesPerMandat);
			sumVotes();
			votesPerMandat=votesForMandat(120-numOfMandats);
			mandatsForParties(votesPerMandat);
			remainingVotes(votesPerMandat);
		}
		sumMandats();
		
		if (numOfMandats!=120)
		{
			remainingMandats=120-numOfMandats;
			for (int i=0;i<remainingMandats;i++)
			{
				mandatsForAllParties[mostRemainingVotes()]++;
				votesForAllParties[mostRemainingVotes()]=0;
			}		
		}
	}
	
	public void publishResults()				//Publish the results
	{
		System.out.println("The results for the elections are below:");
		for (int i = 0; i < parties.length; i++) {
			System.out.println(parties[i] + " : " + mandatsForAllParties[i]
					+ "  Mandats");
		}
		
	}
}
